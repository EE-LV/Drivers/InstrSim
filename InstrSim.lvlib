﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="9008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_0_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.0\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_0_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.0\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is a VI library for the instrument simulator.

NIInstrSimDevice={Hardware=Default, Software}

Copyright (C) 2004 Brand New Technologies; Dr. Holger Brand
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under certain conditions.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">#1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.1.0.0</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">#1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="InstrSim Transaction.vi" Type="VI" URL="../InstrSim Transaction.vi"/>
		<Item Name="InstrSim WrongCmdSyntax.vi" Type="VI" URL="../InstrSim WrongCmdSyntax.vi"/>
		<Item Name="InstrSim Globals.vi" Type="VI" URL="../InstrSim Globals.vi"/>
		<Item Name="InstrSim Globals.ctl" Type="VI" URL="../InstrSim Globals.ctl"/>
		<Item Name="InstrSim ActionEnum.ctl" Type="VI" URL="../InstrSim ActionEnum.ctl"/>
	</Item>
	<Item Name="Typedefs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="InstrSim Command.ctl" Type="VI" URL="../InstrSim Command.ctl"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Config" Type="Folder"/>
		<Item Name="Action/Status" Type="Folder">
			<Item Name="InstrSim Identification.vi" Type="VI" URL="../InstrSim Identification.vi"/>
			<Item Name="InstrSim Test.vi" Type="VI" URL="../InstrSim Test.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="InstrSim MeasureSingle.vi" Type="VI" URL="../InstrSim MeasureSingle.vi"/>
			<Item Name="InstrSim MeasureWaveform.vi" Type="VI" URL="../InstrSim MeasureWaveform.vi"/>
		</Item>
		<Item Name="Examples" Type="Folder">
			<Item Name="InstrSim Getting Started.vi" Type="VI" URL="../InstrSim Getting Started.vi"/>
		</Item>
		<Item Name="Utilities" Type="Folder">
			<Item Name="InstrSim Error Handler.vi" Type="VI" URL="../InstrSim Error Handler.vi"/>
		</Item>
		<Item Name="InstrSim Close.vi" Type="VI" URL="../InstrSim Close.vi"/>
		<Item Name="InstrSim Initialize.vi" Type="VI" URL="../InstrSim Initialize.vi"/>
	</Item>
	<Item Name="Simulator" Type="Folder">
		<Item Name="subVIs" Type="Folder">
			<Item Name="sinechar.vi" Type="VI" URL="../InstrSimulator7x.llb/sinechar.vi"/>
			<Item Name="cmd.vi" Type="VI" URL="../InstrSimulator7x.llb/cmd.vi"/>
			<Item Name="strnglngth.vi" Type="VI" URL="../InstrSimulator7x.llb/strnglngth.vi"/>
		</Item>
		<Item Name="InstrSimulator.vi" Type="VI" URL="../InstrSimulator7x.llb/InstrSimulator.vi"/>
	</Item>
	<Item Name="InstrSim VI Tree.vi" Type="VI" URL="../InstrSim VI Tree.vi"/>
	<Item Name="Gnu GPL V3.txt" Type="Document" URL="../../../User/Brand/Gnu GPL V3.txt"/>
</Library>
